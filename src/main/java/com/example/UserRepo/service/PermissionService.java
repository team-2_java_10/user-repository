package com.example.UserRepo.service;

import com.example.UserRepo.entity.Permission;
import com.example.UserRepo.exceptions.ResourceNotFoundException;
import com.example.UserRepo.repository.PermissionRepository;
import com.example.UserRepo.request.PermissionRequest;
import com.example.UserRepo.reponses.ResponseObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PermissionService {
    private static Logger logger = LogManager.getLogger(PermissionService.class);

    @Autowired
    private PermissionRepository permissionRepository;

    public ResponseEntity<ResponseObject> setPermission(List<PermissionRequest> request) {
        Permission permission = null;
        for (PermissionRequest p:request) {
            try {
                permission = permissionRepository.findById(p.getPermissionID()).get();
                if(permission.getPermissionId() != null){
                    logger.info("Set Permission");
                    permission.setSyllabus(p.getSyllabus());
                    permission.setTrainingProgram(p.getTrainingProgram());
                    permission.setClassManagement(p.getClassManagement());
                    permission.setLearningMaterial(p.getLearningMaterial());
                    permission.setUserManagement(p.getUserManagement());
                    permissionRepository.save(permission);
                }else{
                    throw new ResourceNotFoundException("Permission is found");
                }
            } catch (Exception e) {
                logger.error(e + ": Fail to set permission.");
                return ResponseEntity.badRequest().body(new ResponseObject(1, "Failed to set permission", e.getMessage()));
            }
        }
        return ResponseEntity.ok(new ResponseObject(0, "Permission set successfully", permission));
    }
}
