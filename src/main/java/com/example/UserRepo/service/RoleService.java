package com.example.UserRepo.service;

import com.example.UserRepo.entity.Permission;
import com.example.UserRepo.entity.Role;
import com.example.UserRepo.enums.UserRole;
import com.example.UserRepo.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public Role getRoleByRoleId(Integer roleId) {
        return roleRepository.findByRoleId(roleId).get();
    }

    public List<Role> getAll(){
        return roleRepository.findAll();
    }

    public Role getRoleByRoleName(UserRole roleName){
        return roleRepository.findRoleByRoleName(roleName).get();
    }

}
