package com.example.UserRepo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UserRepoApplication {
	public static void main(String[] args) {
		SpringApplication.run(UserRepoApplication.class, args);
	}

}
